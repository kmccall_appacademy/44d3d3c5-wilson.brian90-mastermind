
class Code
  attr_reader :PEGS
  attr_reader :pegs
  attr_reader :code
  attr_accessor :peg_colors
  PEGS = {Red: "R", Green: "G", Blue: "B", Yellow: "Y", Orange: "O", Purple: "P"}

  def [](idx)
    @pegs[idx]
  end

  def ==(other_code)
    if other_code.class != Code
      return nil
    end

    self.pegs == other_code.pegs
  end

  def initialize(code)

    @pegs = code
  end

  def exact_matches(foreign_code)
    if @pegs.class == Hash
      @pegs = @pegs.values
    end

    match_indices = []
    @pegs.each_index do |idx|
      match_indices << idx if @pegs[idx] == foreign_code[idx]
    end
    if match_indices.size == 0
      return 0
    end
    match_indices.size
  end

  def near_matches(foreign_code)
    foreign_pegs = foreign_code.pegs.clone()
    pegs_clone = @pegs.clone

    pegs_clone.each_index do |idx|
      if pegs_clone[idx] == foreign_pegs[idx]
        pegs_clone.delete_at(idx)
        foreign_pegs.delete_at(idx)
        redo
      end
    end

    p "after deleting exact matches"
    p pegs_clone.to_s
    p foreign_pegs.to_s

    near_match = []
    pegs_clone.each_index do |idx|
        if foreign_pegs.include?(pegs_clone[idx])
          near_match << pegs_clone[idx]
          delete_first_instance(foreign_pegs, pegs_clone[idx])
        end
    end
    p "near matches = " + near_match.to_s
    near_match.size
  end

  def delete_first_instance(array, i)
    array.each_index do |idx|
      if array[idx] == i
        array.delete_at(idx)
        puts "deleted instance of #{i}"
        return nil
      end
    end
    return nil
  end

  def self.parse(str)
    array = str.upcase.chars
    code = []
    array.each_with_index do |char, idx|
      code[idx] = char_to_color(char)
    end
    return Code.new(code)
  end

  def self.char_to_color(char)
    PEGS.each do |k, v|
      return k if v == char
    end
    raise "Invalid color"
  end

  def self.random
    code = {}
    (0..3).each {|peg| code[peg] = rand_color}
    Code.new(code)
  end

  def self.rand_color
    random_index = (rand * 6).to_i
    PEGS.keys[random_index]
  end

end

class Game

  attr_accessor :secret_code
  attr_accessor :turns

  def initialize(code = nil)
    if code == nil
      @secret_code = Code.random
    else
      @secret_code = code
    end


    @turns = 0
  end

  def get_guess
    puts "Guess a color combination, like 'RGBY'"
    str = gets.chomp
    Code.parse(str)
  end

  def display_matches(code)
    puts @secret_code.exact_matches(code)
    puts @secret_code.near_matches(code)
  end


  def play
    while turns < 11 do
      guess = get_guess
      display_matches(guess)
      @turns += 1
    end
  end

  attr_reader :secret_code
end
